package utils;

import java.util.ArrayList;

/**
 * Class representing a simple implementation of
 * weighted undirected graphs.
 * Vertices are simply integers. We store the edges in
 * adjacency list format.
 * @author Charilaos Skiadas
 *
 */
public class WeightedGraph {
	public static class Edge implements Comparable<Edge> {
		public int v1;
		public int v2;
		public int weight;
		
		public Edge(int v1, int v2, int weight) {
			this.v1 = Math.min(v1, v2);
			this.v2 = Math.max(v1, v2);
			this.weight = weight;
		}
		
		public boolean isAdjacentTo(int v) {
			return this.v1 == v || this.v2 == v;
		}
		
		public int getOtherVertex(int v) {
			return this.v1 == v ? v2 : v1;
		}

		@Override
		public int compareTo(Edge other) {
			if (this.weight != other.weight) {
				return this.weight - other.weight;
			}
			if (this.v1 != other.v1) {
				return this.v1 - other.v1;
			}

			return this.v2 - other.v2;
		}
		
		public String toString() {
			return String.format("%d-(%d)-%d", this.v1, this.weight, this.v2);
		}
	}
	
	private DLList<Edge>[] edges; 
	
	/**
	 * Create an empty simple graph with specified order
	 */
	public WeightedGraph(int order) {
		this.edges = (DLList<Edge>[]) new DLList[order];
		for (int i = 0; i < this.edges.length; i += 1) {
			this.edges[i] = new DLList<Edge>();
		}
	}

	public Edge getEdge(int i, int j) {
		this.checkValidVertex(i);
		this.checkValidVertex(j);

		for (Edge edge : this.edges[i]) {
			if (edge.isAdjacentTo(j)) {
				return edge;
			}
		}
		
		return null;
	}
	
	public boolean hasEdge(int i, int j) {
		return getEdge(i, j) != null;
	}
	/**
	 * Add an edge from one vertex to another, if one does not already exist.
	 * @param i     the source vertex
	 * @param j     the target vertex
	 */
	public void addEdge(int i, int j, int weight) {
		this.checkValidVertex(i);
		this.checkValidVertex(j);

		if (!hasEdge(i, j)) {
			Edge edge = new Edge(i, j, weight);
			
			this.edges[i].add(edge);
			this.edges[j].add(edge);
		}
	}
	
	public void addEdge(Edge edge) {
		this.addEdge(edge.v1, edge.v2, edge.weight);
	}
		
	public int order() {
		return this.edges.length;
	}
	
	public DLList<Edge>outgoingEdges(int i) {
		this.checkValidVertex(i);
		
		return this.edges[i]; // Note: for safety we should have cloned
	}
	/**
	 * Check if the vertex index is a valid index for the graph.
	 * @param i     the vertex index
	 * @throws IndexOutOfBoundsException
	 */
	private void checkValidVertex(int i) {
		if (i < 0 || i >= this.edges.length) {
			throw new IndexOutOfBoundsException(String.format("%d",i));
		}
	}
}
