import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.ListIterator;

import org.junit.*;
import utils.DLList;

public class DLListTest {
    @Test 
    public void testNewListIsEmpty() {
        DLList<String> list = new DLList<String>();
        assertTrue("A new list is empty", list.isEmpty());
    }
    
    @Test
    public void testAddFirstRemoveFirstWork() {
        DLList<String> list = new DLList<String>();
        list.addFirst("foo");
        String removedElement = list.removeFirst();
        assertTrue("Adding and removing keeps list empty", list.isEmpty());
        assertEquals("Removed element matches added element", "foo", removedElement);
        list.addFirst("one");
        list.addFirst("two");
        String firstOut = list.removeFirst();
        String secondOut = list.removeFirst();
        assertTrue("Adding and removing keeps list empty", list.isEmpty());
        assertEquals("Removal order agrees with insertion order", "two", firstOut);
        assertEquals("Removal order agrees with insertion order", "one", secondOut);        
    }
    
    @Test
    public void testAddLastRemoveFirstWork() {
        DLList<String> list = new DLList<String>();
        list.addLast("foo");
        String removedElement = list.removeFirst();
        assertTrue("Adding and removing keeps list empty", list.isEmpty());
        assertEquals("Removed element matches added element", "foo", removedElement);
        list.addLast("one");
        list.addLast("two");
        String firstOut = list.removeFirst();
        String secondOut = list.removeFirst();
        assertTrue("Adding and removing keeps list empty", list.isEmpty());
        assertEquals("Removal order agrees with insertion order", "one", firstOut);
        assertEquals("Removal order agrees with insertion order", "two", secondOut);        
    }
    
    @Test
    public void testIteratorWorks() {
        DLList<String> list = new DLList<String>();
        list.addLast("one");
        list.addLast("two");
        list.addLast("three");
        Iterator<String> iterator = list.listIterator();
        assertTrue("First element exists", iterator.hasNext());
        assertEquals("First element correct", "one", iterator.next());    
        assertTrue("Second element exists", iterator.hasNext());
        assertEquals("Second element correct", "two", iterator.next());    
        assertTrue("Third element exists", iterator.hasNext());
        assertEquals("Third element correct", "three", iterator.next());
        assertFalse("No extra elements", iterator.hasNext());
    }

    @Test
    public void testReverseIteratorWorks() {
        DLList<String> list = new DLList<String>();
        list.addLast("one");
        list.addLast("two");
        list.addLast("three");
        Iterator<String> iterator = list.descendingIterator();
        assertTrue("First element exists", iterator.hasNext());
        assertEquals("First element correct", "three", iterator.next());    
        assertTrue("Second element exists", iterator.hasNext());
        assertEquals("Second element correct", "two", iterator.next());    
        assertTrue("Third element exists", iterator.hasNext());
        assertEquals("Third element correct", "one", iterator.next());
        assertFalse("No extra elements", iterator.hasNext());
    }

    @Test
    public void testGetNodeAtIndex() {
        DLList<String> list = new DLList<String>();
        list.addLast("zero");
        list.addLast("one");
        list.addLast("two");
        ListIterator<String> it = list.listIterator(2);
        assertTrue("Iterator starts at correct place", it.hasNext());
        assertEquals("Iterator starts at correct place", "two", it.next());    
        assertFalse("Iterator starts at correct place", it.hasNext());
        
        boolean thrown = false;
        try {
        		it = list.listIterator(4);
        } catch (IndexOutOfBoundsException e) {
        		thrown = true;
        }
        assertTrue("Exception thrown for out of bounds", thrown);
        thrown = false;
        try {
        		it = list.listIterator(-1);
        } catch (IndexOutOfBoundsException e) {
    			thrown = true;
        }
        assertTrue("Exception thrown for out of bounds", thrown);        
    }

    @Test
    public void testSizeWorks() {
        DLList<String> list = new DLList<String>();
        assertEquals("Empty list has size zero", 0, list.size());    
        list.addLast("one");
        assertEquals("Elements counted correctly", 1, list.size());    
        list.addLast("two");
        assertEquals("Elements counted correctly", 2, list.size());    
        list.addLast("three");
        assertEquals("Elements counted correctly", 3, list.size());    
    }
}
